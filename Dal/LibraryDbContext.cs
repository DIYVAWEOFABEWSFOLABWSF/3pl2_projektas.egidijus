﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal.Models;
using MySql.Data.EntityFramework;

namespace Dal
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class LibraryDbContext : DbContext
    {
        public LibraryDbContext()
            : base("library")
        {

        }

        public DbSet<CategoryModel> Categories { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Book> Books { get; set; }

        public DbSet<BookByAuthor> BooksByAuthors { get; set; }

        public DbSet<BookByCategory> BooksByCategories { get; set; }

        public DbSet<BookAtLibrary> BooksAtLibraries { get; set; }

        public DbSet<Library> Libraries { get; set; }

    }
}
