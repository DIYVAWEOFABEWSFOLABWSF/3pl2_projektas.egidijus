﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Models
{
    [Table("authors")]
    public class Author
    {
        [Key]
        public int Id { get; private set; }

        public string Firstname { get; private set; }

        public string Lastname { get; private set; }

        public string Nationality { get; private set; }

        private Author() { }

        public Author(string firstname, string lastname, string nationality)
        {
            Firstname = firstname;
            Lastname = lastname;
            Nationality = nationality;
        }
    }
}
